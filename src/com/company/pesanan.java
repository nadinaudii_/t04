package com.company;

public class pesanan extends kue {
    private double Berat;

    public pesanan(String name, double price, double Berat) {
        //memnaggil atribut parent class
        super(name, price);
        setBerat(Berat);
    }

    public void setBerat(double berat) {
        Berat = berat;
    }

    // total harga dari kue pesanan
    public double hitungHarga() {
        return super.getPrice() * Berat;
    }

    // memanggil method dari parent class
    @Override
    public double Berat() {
        return Berat;
    }

    @Override
    public double Jumlah() {
        return 0;
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public void setPrice(double price) {
        super.setPrice(price);
    }

}
