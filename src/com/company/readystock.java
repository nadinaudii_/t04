package com.company;

public class readystock extends kue {
    private double Jumlah;

    public readystock(String name, double price, double Jumlah) {
        //memanggil atribut parent class
        super(name, price);
        setJumlah(Jumlah);
    }

    public void setJumlah(double jumlah) {
        Jumlah = jumlah;
    }

    // total harga dari kue jadi
    public double hitungHarga() {
        return super.getPrice()*Jumlah*2;
    }

    // memanggil method dari parent class
    @Override
    public double Berat() {
        return 0;
    }

    @Override
    public double Jumlah() {
        return Jumlah;
    }

    @Override
    public double getPrice() {
        return super.getPrice();
    }

    @Override
    public void setPrice(double price) {
        super.setPrice(price);
    }
}
